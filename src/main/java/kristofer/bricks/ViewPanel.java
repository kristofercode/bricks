package kristofer.bricks;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class ViewPanel extends JPanel {

	View view;
	public ViewPanel(View view) {
		super();
		this.view = view;		
	}
			
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
			((Graphics2D)g).drawImage(view.getScaledImage(), 0, 0, this);	
	}
}
