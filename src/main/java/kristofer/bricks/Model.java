package kristofer.bricks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import kristofer.bricks.Model.BlockDirection;
import kristofer.bricks.domain.Ball;
import kristofer.bricks.domain.BallFactory;
import kristofer.bricks.domain.Block;
import kristofer.bricks.domain.BlockFactory;
import kristofer.bricks.domain.Brick;
import kristofer.bricks.domain.Component;
import kristofer.bricks.domain.Wall;
import kristofer.bricks.geometry.Delta;
import kristofer.bricks.geometry.Point;
import kristofer.bricks.geometry.Rectangle;

public class Model {

	public enum BlockDirection {
		LEFT, RIGHT, NONE
	}

	private static final int THICKNESS_OUTREGION = 1000;
	private Wall wall;
	private Ball ball;
	private Block block;
	private Viewport viewport;

	private static final int dxBlock = 7;
	Point wallPos = Point.fromXY(0, 100);

	List<ComponentListener> listeners = new ArrayList<ComponentListener>();
	private Message message;
	private int points;
	private WallFactoryImpl wallFactory;
	private BallFactory ballFactory;
	private BlockFactory blockFactory;

	private BlockDirection blockMoveDirection = BlockDirection.NONE;

	// Refactor thread + fields to "Animator"
	private final int velocityBallPerSec = 200; 
	private final int velocityBlockPerSec = 200;

	private long rateUpdatePerSec = 50;

	private Thread animatorThread = new Thread(() -> {
		while (true) {
			pauseBetweenFrames();

			moveBlock(dxBlock(velocityBlockPerSec), blockMoveDirection);			
		
			updateBallPosition(dxdyBall(velocityBallPerSec));
			
		//	System.out.println(ball.getCenter());
		}
	});
	
	// Shortcut to avoid unnecessary collision to see if ball still in play
	private boolean ballDead;
	
	private void pauseBetweenFrames() {
		try {			
			Thread.sleep(delayBetweenFrameMilliSec());
		} catch (InterruptedException e) {
			// Ignore
		}
	}

	private int dxBlock(int velocityBlockPerSec2) {
		return (int) (velocityBlockPerSec2/(1000.0/delayBetweenFrameMilliSec()));
	}

	private int dxdyBall(int velocityBallPerSec) {
		return (int) (velocityBallPerSec/(1000.0/delayBetweenFrameMilliSec()));
	}

	private int delayBetweenFrameMilliSec() {
		return (int) (1/(rateUpdatePerSec/1000.0));
	}

	private void moveBlock(int dx, BlockDirection direction) {
		if (direction == BlockDirection.LEFT) {
			if (canMoveBlock(-dx)) {
				moveBlock(-dx);
			} else {
				blockMoveDirection = BlockDirection.NONE;
			}
		} else if (direction == BlockDirection.RIGHT) {
			if (canMoveBlock(dx)) {
				moveBlock(dx);	
			} else {
				blockMoveDirection = BlockDirection.NONE;
			}
			
		}		
	}

	public Model(Viewport viewport, BallFactory ballFactory, WallFactoryImpl wallFactory, BlockFactory blockFactory) {
		this.viewport = viewport;
		this.message = new Message("", Point.fromXY(0, viewport.getHeight() + 30));
		this.wallFactory = wallFactory;
		this.ballFactory = ballFactory;
		this.blockFactory = blockFactory;
		

	}
	public void startAnimator() {
		animatorThread.start();
	}

	public Viewport getViewport() {
		return viewport;
	}

	public void addListener(ComponentListener listener) {
		listeners.add(listener);
	}

	public Wall getWall() {
		return wall;
	}

	public Block getBlock() {
		return block;
	}

	public Ball getBall() {
		return ball;
	}


	private void notifyModelUpdated() {
		for (ComponentListener h : listeners) {
			h.updateComponent();
		}
	}

	public void setBlockMovingDirectionLeft() {
		blockMoveDirection = BlockDirection.LEFT;
	}

	public void setBlockMovingDirectionRight() {
		blockMoveDirection = BlockDirection.RIGHT;
	}

	public void moveBlock(int dx) {
		getBlock().moveX(dx);
		notifyModelUpdated();
	}

	public CollisionCheckResult updateBallPosition(double velocityBall) {
		Ball movedProto = calculateMovedProto(velocityBall);

		// Wall collision?
		CollisionCheckResult collisionResult = checkCollideWall(movedProto);
		if (collisionResult.isCollison()) {
			handleCollisionResultModifyBallAngle(ball, collisionResult);
		}
		
		// Border collision?
		if (!collisionResult.isCollison()) {
			if (!viewport.contains(movedProto)) {
				collisionResult = checkCollideBorder(movedProto);
				if (collisionResult.isCollison()) {
					if (collisionResult.getFacet() == CollisionFacet.BOTTOM) {
						// Let ball continue out
						ballDead = true;
					} else {
						handleCollisionResultModifyBallAngle(ball, collisionResult);
					}
					
				}
			}

		}
		
		// Block collision?
		if (!collisionResult.isCollison()) {
			collisionResult= checkCollideBlock(movedProto);
			if (collisionResult.isCollison()) {
				handleCollisionResultModifyBallAngle(ball, collisionResult);
			}
		}

		// Recalculate position due to angle change after collision
		if (collisionResult.isCollison()) {
			movedProto = calculateMovedProto(velocityBall);
		}
		
		// Actually move ball
		ball.setCenter(movedProto.getCenter());

		notifyModelUpdated();
		
		return collisionResult;
	}

	private Ball calculateMovedProto(double vel) {
		
		double x = Math.cos(ball.getAngleDirection());
		double y = -Math.sin(ball.getAngleDirection());

		Point oldPos = ball.getCenter();	
		Point newPos = Point.fromXY(oldPos.getX() + x * vel, oldPos.getY() + y * vel);
		Ball movedProto = ball.movePrototype(newPos);
		return movedProto;
	}

	private void handleCollisionResultModifyBallAngle(Ball ball, CollisionCheckResult collisionResult) {

		double rAng = ball.getAngleDirection();
		double rand = new Random().nextDouble() * 0.01;
		if (collisionResult.isCollison()) {
			switch (collisionResult.getFacet()) {
			case RIGHT:
				ball.setAngleDirection(Math.PI - ball.getAngleDirection() * (1 + rand));
				break;
			case LEFT:
				ball.setAngleDirection(Math.PI - ball.getAngleDirection() * (1 + rand));
				break;
			case BOTTOM:
				ball.setAngleDirection(-ball.getAngleDirection() * (1 + rand));
				break;
			case TOP:
				ball.setAngleDirection(-ball.getAngleDirection() * (1 + rand));
				break;
			default:
				break;

		}
		}

	}

	private CollisionCheckResult checkCollideBlock(Ball ball) {
		boolean isCollide = ball.getCircle().intersects(block.getShape());
		return CollisionCheckResult.create(isCollide, CollisionFacet.BOTTOM, "block");

	}

	private CollisionCheckResult checkCollideBorder(Ball ball) {
		Rectangle rv = viewport.getRectangle();

		Rectangle outerRegionRight = deriveOuterRegionRight(rv);
		CollisionCheckResult resultRiht = checkCollideOuterRegion(ball, outerRegionRight, CollisionFacet.RIGHT);

		if (resultRiht.isCollison()) {
			return resultRiht;
		}
		Rectangle outerRegionLeft = deriveOuterRegionLeft(rv);
		CollisionCheckResult resultLeft = checkCollideOuterRegion(ball, outerRegionLeft, CollisionFacet.LEFT);

		if (resultLeft.isCollison()) {
			return resultLeft;
		}
		Rectangle outerRegionTop = deriveOuterRegionTop(rv);
		CollisionCheckResult resultTop = checkCollideOuterRegion(ball, outerRegionTop, CollisionFacet.TOP);
		if (resultTop.isCollison()) {
			return resultTop;
		}
		Rectangle outerRegionBottom = deriveOuterRegionBottom(rv);
		CollisionCheckResult resultBottom = checkCollideOuterRegion(ball, outerRegionBottom, CollisionFacet.BOTTOM);
		if (resultBottom.isCollison()) {
			return resultBottom;
		}
		return CollisionCheckResult.noCollison();
	}

	private Rectangle deriveOuterRegionBottom(Rectangle rv) {
		return new Rectangle(rv.getLowerLeft(), rv.getLowerRight().translate(Delta.fromXY(0, THICKNESS_OUTREGION)));
	}

	private Rectangle deriveOuterRegionTop(Rectangle rv) {
		return new Rectangle(rv.getUpperLeft().translate(Delta.fromXY(0, -THICKNESS_OUTREGION)), rv.getUpperRight());
	}

	private Rectangle deriveOuterRegionLeft(Rectangle rv) {
		return new Rectangle(rv.getUpperLeft().translate(Delta.fromXY(-THICKNESS_OUTREGION, 0)), rv.getLowerLeft());
	}

	private Rectangle deriveOuterRegionRight(Rectangle rv) {
		return new Rectangle(rv.getUpperRight(), rv.getLowerRight().translate(Delta.fromXY(THICKNESS_OUTREGION, 0)));
	}

	private CollisionCheckResult checkCollideOuterRegion(Ball ball, Rectangle outerRegion,
			CollisionFacet direction) {
		
		boolean isCollide = ball.getCircle().intersects(outerRegion);
		return CollisionCheckResult.create(isCollide, direction, "border-" + direction.name().toLowerCase());
	}

	private CollisionCheckResult checkCollideWall(Ball ball) {
		boolean isCoolide = wall.intersects(ball);
		if (isCoolide) {
			CollisionCheckResult result = CollisionCheckResult.create(isCoolide, CollisionFacet.TOP, "wall");
			if (result.isCollison()) {
				Set<Brick> hitBricks = wall.hitAndDestoryBricks(ball);
				points += hitBricks.size();
			}
			return result;
		} else {
			return CollisionCheckResult.noCollison();
		}
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message mess) {
		this.message = mess;
	}

	public String getPointsMessage() {
		return "Count: " + points;
	}

	public boolean canMoveBlock(int dx) {
		Block movedProto = getBlock().movedPrototypeX(dx);
		boolean isIntersecting = movedProto.intersects(getViewport());
		return !isIntersecting;
	}


	private void InitBall() {
		Point startPos = generateBallStartPosition();
		getBall().setCenter(startPos);
		// getBall().setAngleDirection(calculateAngleDirection(startPos,
		// getBlock().getPosition()));
		double angle = 3.0 / 4.0 * 2.0 * Math.PI;
		getBall().setAngleDirection(angle);
		ballDead = false; 

	}

	private Point generateBallStartPosition() {
		return Point.fromXY(viewport.getWidth() / 2.0, viewport.getHeight() * 2.0 / 3);
	}

	private void initBlock() {
		int blockPosX = (int) (viewport.getWidth() * 0.5);
		int blockPosY = (int) (viewport.getHeight() * 0.9);
		Point blockPos = Point.fromXY(blockPosX, blockPosY);
		getBlock().setPosition(blockPos);
		blockMoveDirection = BlockDirection.NONE;
	}

	public void initComponents() {
		this.ball = ballFactory.createBall();
		this.wall = wallFactory.createWall();
		this.block = blockFactory.createBlock();

		initBlock();

		InitBall();

		initPoints();
		
		fireModelUpdated();
		
		

	}

	private void fireModelUpdated() {
		// TODO Auto-generated method stub
		
	}

	private void initPoints() {
		points = 0;

	}

	public boolean isMovingRight() {
		return blockMoveDirection == BlockDirection.RIGHT;
	}

	public boolean isMovingLeft() {
		return blockMoveDirection == BlockDirection.LEFT;
	}

	public void stopBlock() {
		blockMoveDirection = BlockDirection.NONE;

	}

	public boolean isBallDead() {		
		return ballDead;
	}

	public void setViewport(Viewport viewport) {
		this.viewport = viewport;		
	}
}
