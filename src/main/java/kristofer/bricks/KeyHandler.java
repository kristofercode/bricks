package kristofer.bricks;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

public class KeyHandler implements KeyListener {

	private Controller controller;

	public KeyHandler(Controller controller) {
		this.controller = controller;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		handleEvent(e);
	}

	private void handleEvent(KeyEvent e) {

		switch (e.getKeyChar()) {
		case 'a':
		case 'A':
			controller.pressLeft();
			break;
		case 's':
		case 'S':
			controller.pressRight();
			break;
		case 'w':
		case 'W':
			controller.stopBlock();
				break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
