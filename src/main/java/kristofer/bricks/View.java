package kristofer.bricks;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import kristofer.bricks.domain.Block;

public interface View extends ComponentListener {

	void drawWall();

	void drawBlock();

	void drawBall();

	void drawStatusBar();

	int getViewWidth();

	int getViewHeight();

	void drawComponents();

	void showMessage();

	Image getScaledImage();

	void repaint();

}
