package kristofer.bricks;

import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import kristofer.bricks.domain.BallFactory;
import kristofer.bricks.domain.BallFactoryImpl;
import kristofer.bricks.domain.BlockFactory;
import kristofer.bricks.domain.BrickFactory;
import kristofer.bricks.geometry.Point;
import kristofer.bricks.domain.BrickFactoryImpl;

public class Main {

	static PlayingView view;
	static Controller controller;
	private static int blockWidth  = 50;
	private static int blokcHeight = 15;
	
	public static void initMVC() {		
		
		Viewport viewport = new Viewport(600,600);
		Model model = new Model(viewport, getBallFactoryBean(), getWallFactoryBean(viewport, getBrickFactoryBean()), getBlockFactoryBean(blockWidth, blokcHeight));
		view = new PlayingView(model, viewport);
		controller = new Controller(view, model);		
		
	}
	
	private static WallFactoryImpl getWallFactoryBean(Viewport viewport, BrickFactory brickFactory) {
		return new WallFactoryImpl(viewport, brickFactory);
	}
	private static BlockFactory getBlockFactoryBean(int blockWidth, int blockHeight) {
		return new BlockFactory(blockWidth, blockHeight);
	}
	private static BrickFactoryImpl getBrickFactoryBean() {
		return new BrickFactoryImpl();
	}
	
	private static BallFactory getBallFactoryBean() {
		return new BallFactoryImpl();
	}
	public static void main(String[] args) {
		initMVC();
		
		initFrame();
		
		controller.gameLoop();
	}

	private static void initFrame() {
		JFrame frame = new JFrame();
		//View view= new PlayingView(view);
		frame.addKeyListener(new KeyHandler(controller));
		frame.addMouseListener(new MouseHandler(controller));
		frame.setContentPane(view.getPanel());
		frame.setSize(view.getViewWidth(), view.getViewHeight());
		frame.setVisible(true);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		frame.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				controller.setViewportSize(e.getComponent().getWidth(), e.getComponent().getHeight());
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
