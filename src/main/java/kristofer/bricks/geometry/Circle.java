package kristofer.bricks.geometry;


import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

public class Circle extends Shape {

	
	private java.awt.Shape awtCircle = new java.awt.geom.Ellipse2D.Double();;
	
	private Point center;
	private int radius;
	
	private Point[] periPoints = new Point[12];
	
	
	public Circle(Point center, int radius) {
		this.center =center;
		this.radius = radius;		
		deriveAwtShape();
		
	}
	private void deriveAwtShape() {
		 awtCircle = new Ellipse2D.Double(center.getX(),center.getY(), radius, radius);
		 derivePeriPoints();
	}
	private void derivePeriPoints() {
		
		
		int nSteps = 12;
		double step = 2*Math.PI/nSteps;
		for (int k=0; k < nSteps; ++k) {
			double a = k*step;
			periPoints[k] = Point.fromXY(radius*Math.cos(a), -radius*Math.sin(a));
		}
		
	}
	public Point getCenter() {
		return center;
	}
	public void setCenter(Point center) {
		this.center = center;
		deriveAwtShape();
	}
	public int getRadius() {
		return radius;
	}
	public void setRadius(int radius) {
		this.radius = radius;
		deriveAwtShape();
	}
	public Point[] getPerimeterPoints() {
		return periPoints;
	}
	
	
	@Override
	public boolean contains(Point p) {
		return Math.pow(p.getX()-this.center.getX(),2) + Math.pow(p.getY()- this.center.getY(),2) < Math.pow(radius,2);
	}
	@Override
	public boolean intersects(Rectangle r) {
		return awtCircle.intersects(r.getAwtRectangle()); 
	}

	
}
