package kristofer.bricks.geometry;

import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

public class Rectangle extends Shape {
//
//	private Point upperLeft, lowerRight;
//	private Point upperRight;
//	private Point lowerLeft;
	
	private java.awt.Rectangle awtRect;

	public Rectangle(Point upperLeft, Point lowerRight) {
		int width = (int) (lowerRight.getX()-upperLeft.getX());
		int  height = (int) (lowerRight.getY()-upperLeft.getY());
		awtRect = new java.awt.Rectangle((int)upperLeft.getX(), (int)upperLeft.getY(), width, height);
//		this.upperLeft = upperLeft;
//		this.lowerRight = lowerRight;
//		this.upperRight = calculateUpperRight();
//		this.lowerLeft = calculateLowerLeft();
	}
//
//	private Point calculateLowerLeft() {
//		return Point.fromXY(upperLeft.getX(), lowerRight.getY());
//
//	}
//
//	private Point calculateUpperRight() {
//		return Point.fromXY(lowerRight.getX(), upperLeft.getY());
//	}
//
	public double getWidth() {
		return awtRect.getWidth();
	}

	public double getHeight() {
		return awtRect.getHeight();
	}

	

	public Point getUpperRight() {
		return Point.fromXY(awtRect.getMaxX(), awtRect.getMinY());
	}

	public Point getLowerLeft() {
		return Point.fromXY(awtRect.getMinX(), awtRect.getMaxY());
	}

	public Point getUpperLeft() {
		return Point.fromXY(awtRect.getMinX(), awtRect.getMinY());
	}

	public Point getLowerRight() {
		return Point.fromXY(awtRect.getMaxX(), awtRect.getMaxY());
	}

	@Override
	public String toString() {
		return getUpperLeft() + ", w=" + getWidth() + ", h=" + getHeight();
	}

	public static Rectangle fromUpperLeftSize(Point upperLeft, int width, int height) {
		return new Rectangle(upperLeft, Point.fromXY(upperLeft.getX() + width - 1, upperLeft.getY() + height - 1));
	}

	
	@Override
	public boolean intersects(Rectangle r) {
		return contains(r.getUpperLeft()) || contains(r.getLowerRight()) || contains(r.getUpperRight())
				|| contains(r.getLowerLeft());	
		
	}
	
	@Override
	public boolean contains(Point p) {
		return awtRect.contains(p.getX(), p.getY());
//		return upper.getX() <= p.getX() && p.getX() <= lowerRight.getX() && upperLeft.getY() <= p.getY()
//				&& p.getY() <= lowerRight.getY();
	}
	public Rectangle2D getAwtRectangle() {
		return awtRect;
	}

	

}
