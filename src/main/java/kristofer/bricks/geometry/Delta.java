package kristofer.bricks.geometry;

public class Delta {

	private double x, y;
	
	private Delta() {
		super();
	}
	public static Delta fromXY(double x, double y) {
		Delta d =new Delta();
		d.x  =x;
		d.y = y;
		return d;
	}
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	public static Delta fromPoint(Point p) {
		return fromXY(p.getX(), p.getY());
	}
	
}
