package kristofer.bricks.geometry;

public class Color {

	private java.awt.Color awtColor;
	
	public Color(int r, int g, int b) {
		this.awtColor = new java.awt.Color(r, g, b);
	}
	
	Color(java.awt.Color awtColor) {
		this.awtColor = awtColor;
	}


	public java.awt.Color getAwtColor() {
		return awtColor;
	}


}

