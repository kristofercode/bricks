package kristofer.bricks.geometry;

public abstract class Shape {
	
	public abstract boolean intersects(Rectangle r);

	public abstract boolean contains(Point p);
	public boolean containsAnyPoint(Point...points) {
		if (points.length <= 1) {
			throw new IllegalArgumentException("Shape.containsPoints should only be called with >1 points. Use Shape.contains.");
		}
		for (Point p : points) {
			if (this.contains(p)) {
				return true;
			}
		}
		return false;
		
	}
	
}
