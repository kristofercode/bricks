package kristofer.bricks.geometry;

import kristofer.bricks.geometry.Delta;

public class Point {

	public static final Point ORIGO = Point.fromXY(0, 0);
	private double x;
	private double  y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}


	public static Point fromXY(double x, double y) {
		return new Point(x,y);
	}
	
	@Override
	public String toString() {
		return "(x=" + x + ", y=" + y  +")";
	}

	public java.awt.Point fromAwtPoint(Point p) {
		return new java.awt.Point((int)p.getX(), (int)p.getY());
	}

	public Point translate(Delta delta) {
		return Point.fromXY(this.x + delta.getX(), this.y + delta.getY());
	}
}
