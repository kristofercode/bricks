package kristofer.bricks;

/**
 * Meant to be used for specifying which direction the collision happened. It is relative depending on what actor is calculating the collision.
 * @author A548614
 *
 */
public enum CollisionFacet {

	RIGHT, LEFT, BOTTOM, TOP, NONE;
}
