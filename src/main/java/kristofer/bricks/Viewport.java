package kristofer.bricks;

import kristofer.bricks.domain.Ball;
import kristofer.bricks.domain.Block;
import kristofer.bricks.domain.Component;
import kristofer.bricks.geometry.Point;
import kristofer.bricks.geometry.Rectangle;

public class Viewport {

	private int height;
	private int width;
	private Rectangle rectangle;
	
	public Viewport(int w, int h) {
		this.height = h;
		this.width = w;
		updateShape();
	}
	
	private void updateShape() {
		this.rectangle = Rectangle.fromUpperLeftSize(Point.fromXY(0, 0), width, this.height);		
	}

	public Rectangle getRectangle() {
		return rectangle;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	
	@Override
	public String toString() {
		return "viewport: " + getRectangle();
	}

	public boolean contains(Ball ball) {
		Point[] pointsAtBorder = new Point[] {
				Point.fromXY(ball.getX()+ball.getRadius(), ball.getY()),
				Point.fromXY(ball.getX(), ball.getY()+ball.getRadius()),
				Point.fromXY(ball.getX()-ball.getRadius(), ball.getY()),
				Point.fromXY(ball.getX(), ball.getY()-ball.getRadius())
		};
		boolean containsAllPoints = true;
		for (Point p : pointsAtBorder) {
			containsAllPoints &= getRectangle().contains(p);
		}
		return containsAllPoints;		
	}

	public Rectangle getShape() {
		return rectangle;
	}

	
}
