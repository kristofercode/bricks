package kristofer.bricks;

import kristofer.bricks.domain.Brick;
import kristofer.bricks.domain.BrickFactory;
import kristofer.bricks.domain.Wall;
import kristofer.bricks.geometry.Point;

public class WallFactoryImpl implements WallFactory {

	private BrickFactory brickFactory;
	private Viewport viewport;

	public WallFactoryImpl(Viewport viewport, BrickFactory brickFactory) {
		this.brickFactory = brickFactory;
		this.viewport = viewport;
	}
	
	public Wall createFromWidthHeightNXNY(int width, int height, int nx, int ny, int yOffset) {
		
		int brickWidth = (int) ((1.0 * width) / nx);
		int brickHeight = (int) ((1.0 * height) / ny);

		Wall wall = new Wall();
		for (int kx = 0; kx < nx; ++kx) {
			for (int ky = 0; ky < ny; ++ky) {
				Point upperLeft = new Point(kx * brickWidth, ky * brickHeight +yOffset);
				Brick brick = brickFactory.createBrickFromUpperLeftSize(upperLeft, brickWidth, brickHeight);				
				wall.addBrick(brick);
			}
		}
		return wall;
	}

	public Wall createWall() {
		return createFromViewportExtents(75);		
	}

	private Wall createFromViewportExtents(int yOffset) {
		int wallWidth= viewport.getWidth();
		int wallHeight= (int)(viewport.getHeight()*0.4);
		int nx = 10, ny = 15;
			return createFromWidthHeightNXNY(wallWidth, wallHeight, nx, ny, yOffset);
	}

	
}
