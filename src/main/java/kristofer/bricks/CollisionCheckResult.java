package kristofer.bricks;

/**
 * Contains the result of a collison check
 * @author A548614
 *
 */
public class CollisionCheckResult {

	private boolean isCollision;
	private String object;
	private CollisionFacet facet = CollisionFacet.NONE;
	
	private CollisionCheckResult(boolean isCollide, CollisionFacet facet, String object) {
		this.isCollision = isCollide;
		this.object = object;
		this.facet = facet;
	}

	public String getObject() {
		return object;
	}
	public boolean isCollison() {
		return isCollision;
	}
	

	public static CollisionCheckResult create(boolean isCollide, CollisionFacet facet, String object) {
		return new CollisionCheckResult(isCollide, facet, object);
		
	}

	public static CollisionCheckResult noCollison() {
		return CollisionCheckResult.create(false, CollisionFacet.NONE, "");
		
	}

	public CollisionFacet getFacet() {
		return facet;
	}

}
