package kristofer.bricks;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kristofer.bricks.domain.Brick;
import kristofer.bricks.geometry.Point;

/**
 * Coordinates the model and view. Contains the game loop.
 * 
 * @author A548614
 *
 */
public class Controller {

	private View view;

	private Model model;

	Logger logger = LoggerFactory.getLogger(Controller.class);
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;

	}

	public void gameLoop() {
		
		model.initComponents();
		model.startAnimator();
		
		while (true) {
			pause(200);
			logger.debug(model.getBall().toString());
			if (model.isBallDead()) {
				logger.debug("ball out");
				pauseBeforeReinit();
				model.initComponents();
				logger.debug("ball reinited");
			}

		}
	}

	private void pauseBeforeReinit() {

	pause(1000);

	}

	private void pause(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void mouseClicked(int x, int y) {
		Optional<Brick> brick = model.getWall().getBrickAtXY(Point.fromXY(x, y));
		if (brick.isPresent()) {
			model.setMessage(new Message("Selected brick: " + brick.get().getBoundedRect(),
					brick.get().getBoundedRect().getLowerRight()));

		}

	}

	public void pressLeft() {
		model.setBlockMovingDirectionLeft();
	}

	public void pressRight() {
		model.setBlockMovingDirectionRight();
	}

	public void stopBlock() {
		model.stopBlock();
	}

	public void setViewportSize(int width, int height) {
		model.setViewport(new Viewport(height, width));
		
		
	}

}
