package kristofer.bricks;

import kristofer.bricks.domain.Component;

public interface ComponentListener {

	public void updateComponent();
}
