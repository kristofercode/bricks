package kristofer.bricks;

import kristofer.bricks.geometry.Point;

public class Message {

	private String message = "";
	private Point pos;
	public Message(String message, Point pos) {
		this.message = message;
		this.pos = pos;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Point getPos() {
		return pos;
	}
	public void setPos(Point pos) {
		this.pos = pos;
	}
	
	
}
