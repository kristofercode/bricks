package kristofer.bricks;

import kristofer.bricks.domain.Wall;

public interface WallFactory {

	Wall createWall();
}
