package kristofer.bricks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import kristofer.bricks.domain.Ball;
import kristofer.bricks.domain.Block;
import kristofer.bricks.domain.Brick;
import kristofer.bricks.domain.Component;
import kristofer.bricks.domain.Wall;

public class PlayingView implements View {

	private static final Color bgColor = Color.BLACK;

	private Model model;
	private Viewport viewport;

	private Image image;
	private Image scaledImage;
	private ViewPanel panel = new ViewPanel(this);

	public PlayingView(Model model, Viewport viewport) {
		this.model = model;
		this.model.addListener(this);
		this.viewport = viewport;
		image = new BufferedImage(viewport.getWidth(), viewport.getHeight(), BufferedImage.TYPE_INT_RGB);
	}

	@Override
	public void drawWall() {
		Wall wall = getModel().getWall();
		Graphics2D g = getGraphics();
		for (Brick brick : wall.getBricks()) {
			brick.paint(g);
		}
		g.dispose();
	}

	private Graphics2D getGraphics() {
		return ((Graphics2D) image.getGraphics());
	}

	private Model getModel() {
		return model;
	}

	public void drawPointMessage() {
		String pointMessage = getModel().getPointsMessage();
		Graphics2D g = getGraphics();
		g.setBackground(bgColor);
		g.setColor(Color.white);
		g.drawString(pointMessage, 30, 30);
		g.dispose();
	}

	@Override
	public void drawBlock() {
		Block block = getModel().getBlock();
		Graphics2D g = getGraphics();
		g.setBackground(bgColor);
		block.paint(g);
		g.dispose();

	}

	@Override
	public void drawBall() {
		Ball ball = getModel().getBall();
		Graphics2D g = getGraphics();
		ball.paint(g);
		g.dispose();
	}

	@Override
	public void drawStatusBar() {

	}

	@Override
	public int getViewWidth() {
		return viewport.getWidth();
	}

	@Override
	public int getViewHeight() {
		return viewport.getHeight();
	}

	@Override
	public void updateComponent() {
		drawComponents();
	}

	public JPanel getPanel() {
		return panel;
	}

	// TODO: replace w template method taking g as argument?
	@Override
	public void drawComponents() {

		if (panel.getWidth() != 0 && panel.getHeight() != 0) {
			Graphics2D g = getGraphics();
			g.clearRect(0, 0, viewport.getWidth(), viewport.getHeight());
			drawBall();
			drawBlock();
			drawWall();
			showMessage();
			drawPointMessage();
			g.dispose();

			scaledImage = image.getScaledInstance(panel.getWidth(), panel.getHeight(), Image.SCALE_FAST);
			repaint();
		}
	}

	@Override
	public void showMessage() {
		Graphics2D g = getGraphics();
		g.drawString(model.getMessage().getMessage(), (int) model.getMessage().getPos().getX(),
				(int) model.getMessage().getPos().getY());
		g.dispose();
	}

	@Override
	public Image getScaledImage() {
		return scaledImage;
	}

	@Override
	public void repaint() {
		panel.repaint();

	}
}
