package kristofer.bricks.domain;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import kristofer.bricks.geometry.Delta;
import kristofer.bricks.geometry.Circle;
import kristofer.bricks.geometry.Color;
import kristofer.bricks.geometry.Point;

public class Ball implements Component {
	
	private static final Color COLOR_DEFAULT = new Color(200,200,200);
	private Color color = COLOR_DEFAULT;

	private double angleRadiansDirection;
	
	private Circle circle;

	public Ball(Point center, int radius) {
		this.circle = new Circle(center, radius);
	}

	public Color getColor() {
		return color;
	}

	public double getAngleDirection() {
		return angleRadiansDirection;
	}

	public void setAngleDirection(double angleDirection) {
		this.angleRadiansDirection = angleDirection;
	}

	public void setCenter(Point center) {
		circle.setCenter(center);
	}

	public Circle getCircle() {
		return circle;
	}

	public double getX() {
		return circle.getCenter().getX();
	}

	public double getY() {
		return circle.getCenter().getY();
	}

	public int getRadius() {
		return circle.getRadius();
	}

	@Override
	public Circle getShape() {
		return circle;
	}

	public Point getCenter() {
	return circle.getCenter();
	}
//
//	public void updateBallPosition(double vel) {
//		double x = Math.cos(getAngleDirection());
//		double y = Math.sin(getAngleDirection());
//		
//		Point oldPos = getCenter();
//		setCenter(Point.fromXY(oldPos.getX() +x*vel, oldPos.getY() +y*vel));
//	}

	public Ball movePrototype(Point newPos) {
		Ball proto = new Ball(newPos, this.getRadius());
		return proto;
	}
	@Override
	public String toString() {
		return getCenter() + " radius: " + getRadius();
	}

	public Point frontingPoint() {
		double r = getRadius();
		double a = getAngleDirection();
		return Point.fromXY(r*Math.cos(a), r*Math.sin(a)).translate(Delta.fromPoint(getCenter()));
	}

	public void paint(Graphics2D g) {
		g.fill(new Ellipse2D.Double(getX() - getRadius(), getY() - getRadius(), getRadius()*2,
				getRadius()*2));
		
	}



}
