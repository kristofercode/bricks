package kristofer.bricks.domain;

import java.awt.Color;
import java.awt.Graphics2D;

import kristofer.bricks.Viewport;
import kristofer.bricks.geometry.Point;
import kristofer.bricks.geometry.Rectangle;

public class Block implements Component {

	private Point pos = Point.ORIGO;
	private Rectangle rectangle;
	private int width;
	private int height;

	public Block(int width, int height) {
		this.width = width;
		this.height = height;
		initRect();
	}

	@Override
	public Rectangle getShape() {
		return rectangle;
	}

	private void initRect() {
		this.rectangle = Rectangle.fromUpperLeftSize(this.pos, this.width, this.height);
		
	}

	public int getX() {
		return (int) pos.getX();
	}

	public int getY() {
		return (int) pos.getY();
	}

	public void setX(double x) {
		pos = Point.fromXY(x,pos.getY());
	}

	public Point getPosition() {
		return pos;
	}

	

	public boolean intersects(Viewport viewport) {
		return !(viewport.getShape().contains(this.getShape().getUpperLeft())
				&& viewport.getShape().contains(this.getShape().getUpperRight())
				&& viewport.getShape().contains(this.getShape().getLowerLeft())
				&& viewport.getShape().contains(this.getShape().getLowerRight()));
	}

	

	public int getWidth() {
		return (int) rectangle.getWidth();
	}

	public int getHeight() {
		return (int) rectangle.getHeight();
	}

	public void setPosition(Point pos) {
		this.pos = pos;
		initRect();
	}

	public void moveX(int dx) {
		pos=Point.fromXY(pos.getX() + dx,pos.getY());
		initRect();
		
	}

	public Block movedPrototypeX(int dx) {
		Block b = Block.fromPrototype(this);
		b.moveX(dx);
		return b;
	}

	public static Block fromPrototype(Block block) {
		Block b = new Block(block.getWidth(), block.getHeight());
		b.setPosition(block.getPosition());
		return b;
	}

	public void paint(Graphics2D g) {
		g.setColor(Color.white);
		g.fill(new java.awt.Rectangle(getX(), getY(), getWidth(), getHeight()));
		
	}

}
