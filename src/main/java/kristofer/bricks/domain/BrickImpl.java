package kristofer.bricks.domain;

import java.awt.Graphics2D;

import kristofer.bricks.geometry.Color;
import kristofer.bricks.geometry.Point;
import kristofer.bricks.geometry.Rectangle;
import kristofer.bricks.geometry.Shape;

public class BrickImpl implements Brick{

	private boolean destroyed;
	private Rectangle boundedRect;
	private Color color;

	public BrickImpl(Point upperLeft, int width, int height, Color color) {
		this.boundedRect = Rectangle.fromUpperLeftSize(upperLeft, width,height);
		this.color = color;
	}

	@Override
	public boolean isDestroyed() {
		return destroyed;
	}

	@Override
	public Rectangle getBoundedRect() {
		return boundedRect;
	}

	public void setBoundedRect(Rectangle boundedRect) {
		this.boundedRect = boundedRect;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	
	@Override
	public void paint(Graphics2D g) {
		Rectangle r = getBoundedRect();
		g.setColor(getColor().getAwtColor());
		if (!isDestroyed()) {
			g.fillRect((int)r.getUpperLeft().getX(), (int)r.getUpperLeft().getY(), (int)r.getWidth(), (int)r.getHeight());	
		}
		

	}

	@Override
	public void setDestoryed() {
		this.destroyed = true;
		
	}
	
	public String toString() {
		return getBoundedRect().toString();
	}


	@Override
	public Shape getShape() {
		return boundedRect;
	}


	
	

}
