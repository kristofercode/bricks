package kristofer.bricks.domain;

import java.util.Random;

import kristofer.bricks.geometry.Color;
import kristofer.bricks.geometry.Palettes;
import kristofer.bricks.geometry.Point;

public class BrickFactoryImpl implements BrickFactory {

	private Color[] availColors = Palettes.grayShades;
	
	public BrickFactoryImpl() {
		
	}

	@Override
	public Brick createBrickFromUpperLeftSize(Point upperLeft, int width, int height) {		
		return new BrickImpl(upperLeft, width, height, randomColor());
	}

	private Color randomColor() {
		int i = new Random().nextInt(availColors.length);
		return availColors[i];
	}

}
