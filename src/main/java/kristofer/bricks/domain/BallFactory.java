package kristofer.bricks.domain;

public interface BallFactory {

	Ball createBall();

}
