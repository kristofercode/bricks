package kristofer.bricks.domain;

import kristofer.bricks.geometry.Point;

public interface BrickFactory {
	
	Brick createBrickFromUpperLeftSize(Point upperLeft, int width, int height);
	
}
