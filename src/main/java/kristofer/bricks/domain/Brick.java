package kristofer.bricks.domain;

import java.awt.Graphics2D;

import kristofer.bricks.geometry.Point;
import kristofer.bricks.geometry.Rectangle;

public interface Brick extends Component {

	boolean isDestroyed();
	
	Rectangle getBoundedRect();
	
	void paint(Graphics2D g);

	void setDestoryed();

	default boolean contains(Point point) {
		return getBoundedRect().contains(point);
	}

	default Point getUpperLeft() {
		return getBoundedRect().getUpperLeft();
	}

	default public int getX() {
		return (int) getBoundedRect().getUpperLeft().getX();
	}
	default public int getY() {
		return (int) getBoundedRect().getUpperLeft().getY();
	}	
	default public int getWidth() {
		return (int) getBoundedRect().getWidth();
	}	
	default public int getHeight() {
		return (int) getBoundedRect().getHeight();
	}	
}
