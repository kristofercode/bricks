package kristofer.bricks.domain;

public class BlockFactory {

	private int w;
	private int h;

	public BlockFactory(int w, int h) {
		this.w = w;
		this.h = h;
	}

	public Block createBlock() {
		Block block = new Block(this.w, this.h);
		return block;
	}

}
