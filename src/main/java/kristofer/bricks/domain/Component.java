package kristofer.bricks.domain;

import java.awt.geom.AffineTransform;

import kristofer.bricks.geometry.Shape;

public interface Component {

	Shape getShape();
	
}
