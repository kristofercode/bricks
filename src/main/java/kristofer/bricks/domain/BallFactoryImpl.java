package kristofer.bricks.domain;

import kristofer.bricks.geometry.Point;

public class BallFactoryImpl implements BallFactory {

	private int radius = 8;

	public BallFactoryImpl() {
	}

	@Override
	public Ball createBall() {		
		Point ballCenter = Point.fromXY(0,0);
		return new Ball(ballCenter, radius);
	}
}
