package kristofer.bricks.domain;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import kristofer.bricks.geometry.Point;

public class Wall {

	private Set<Brick> bricks = new LinkedHashSet<>();

	private Point upperLeft;

	public Set<Brick> hitAndDestoryBricks(Ball ball) {
		Point p = ball.frontingPoint();
		Set<Brick> destroyed = new HashSet<>();
		for (Brick b : bricks) {
			boolean hit = b.contains(p);
			if (hit) {
				b.setDestoryed();
				destroyed.add(b);
			}
		}
		return destroyed;
	}

	public void addBrick(Brick brick) {
		bricks.add(brick);

	}

	public Optional<Brick> getBrickAtXY(Point point) {
		for (Brick b : bricks) {
			if (b.contains(point)) {
				return Optional.of(b);
			}
		}
		return Optional.empty();
	}

	public int count() {
		return bricks.size();
	}
	

	public Set<Brick> getBricks() {
		return bricks;
	}

	@Override
	public String toString() {
		return asString(bricks);
	}

	public static String asString(Set<Brick> bricks) {
		StringBuilder sb = new StringBuilder();
		for (Brick b : bricks) {
			sb.append(b.toString());
			sb.append("   ");
		}
		return sb.toString();
	}

	public Point getUpperLeft() {
		return upperLeft;
	}

	public void setUpperLeft(Point upperLeft) {
		this.upperLeft = upperLeft;
	}

	public boolean intersects(Ball ball) {

		for (Brick b : bricks) {
			if (!b.isDestroyed()) {
				boolean intersect = b.getBoundedRect().contains(ball.getCenter()); //ball.getCircle().getPerimeterPoints());
				//ball.getShape().intersects(b.getBoundedRect());
				if (intersect) {
					return true;
				}
			}
		}
		return false;
	}

}
